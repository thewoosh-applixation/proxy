CS = mcs

PROGRAM = Program.cs
SOURCES = $(PROGRAM) Logger.cs SecurityDetails.cs HTTPClient.cs HTTPServer.cs ProxyWrapper.cs

OUTPUT_DIRECTORY = build
OUTPUT_EXECUTABLE = Program.exe

$(OUTPUT_DIRECTORY)/$(OUTPUT_EXECUTABLE): $(SOURCES)
	@mkdir -p $(OUTPUT_DIRECTORY)
	$(CS) $(SOURCES) -out:$@
