using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;

class ProxyWrapper
{

    internal string Address;
    internal ushort Port;

    internal TcpClient tcpClient;
    internal Stream stream;

    internal StreamReader Reader;
    internal StreamWriter Writer;

    public ProxyWrapper(string address, ushort port)
    {
        Address = address;
        Port = port;
    }

    public bool Connect(StreamWriter logWriter)
    {
        try
        {
            tcpClient = new TcpClient(Address, Port);
            if (Port == 443)
            {
                SslStream sslStream = new SslStream(tcpClient.GetStream());
                sslStream.AuthenticateAsClient(Address);
                stream = sslStream;
            }
            else
            {
                stream = tcpClient.GetStream();
            }
        }
        catch
        {
            logWriter.WriteLine("ERROR(ProxyWrapper::Connect) Failed to setup connection");
            return false;
        }

        Reader = new StreamReader(stream);
        Writer = new StreamWriter(stream);

        Writer.AutoFlush = true;

        return true;
    }

    public void Dispose()
    {
        try
        {
            Reader.Close();
            Writer.Close();
            stream.Close();
            tcpClient.Close();
        }
        catch
        {
        }
    }

    public bool Interchange(StreamReader clientReader,
                            StreamWriter clientWriter,
                            StreamWriter logWriter)
    {
        return InterchangeMessage(clientReader, this.Writer, logWriter)
            && InterchangeMessage(this.Reader, clientWriter, logWriter);
    }

    private static bool InterchangeMessage(StreamReader origin, StreamWriter destination, StreamWriter logWriter)
    {
        string requestLine = origin.ReadLine();
        destination.WriteLine(requestLine);
        logWriter.WriteLine("Chunk \"" + requestLine + '"');

        int clientContentLength = 0;
        
        string headerLine;
        while ((headerLine = origin.ReadLine()) != null)
        {
            if (headerLine.ToLower().StartsWith("Content-Length"))
            {
                clientContentLength = int.Parse(headerLine.Substring(headerLine.IndexOf(':') + 2));
            }

            destination.WriteLine(headerLine);
            logWriter.WriteLine("Chunk \"" + headerLine + '"');

            if (headerLine.Length == 0)
            {
                break;
            }
        }

        if (!TransferMessageBody(origin, destination, clientContentLength, logWriter))
        {
            logWriter.WriteLine("ERROR(InterchangeMessage) Failed transfer message body");
            return false;
        }

        return true;
    }

    private static bool TransferMessageBody(StreamReader origin, StreamWriter destination, int dataLength, StreamWriter logWriter)
    {
        char[] chunk = new char[1024];
        do {
            int read = origin.Read(chunk, 0, (dataLength < chunk.Length ? dataLength : chunk.Length));
            if (read <= 0)
            {
                logWriter.WriteLine("ERROR(TransferMessageBody) Failed read from origin.");
                return false;
            }
            dataLength -= read;

            destination.Write(chunk, 0, read);
            logWriter.Write("Message body chunk: \"");
            logWriter.Write(chunk, 0, read);
            logWriter.Write("\"\n");
        } while (dataLength != 0);

        return true;
    }

}