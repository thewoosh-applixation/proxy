using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

public class SecurityDetails
{
    public readonly X509Certificate2 Certificate;
    public readonly SslProtocols SecurityProtocols = SslProtocols.Tls12;

    public SecurityDetails(string certificatePath, string password)
    {
        Certificate = new X509Certificate2(certificatePath, password);
    }

}