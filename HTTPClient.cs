using System;
using System.IO;

public class HTTPClient
{

    public const byte LF = 0x0A;
    public static readonly byte[] NewLineBytes = new byte[]{ LF };

    internal Stream Stream;
    internal StreamWriter Writer;
    internal StreamReader Reader;

    internal FileStream logFile;
    internal StreamWriter logFileWriter;

    // proxy connection
    internal string ProxyAddress;
    internal ushort ProxyPort;
    internal ProxyWrapper proxyWrapper;

    public HTTPClient(Stream stream, string logFileName)
    {
        Stream = stream;
        Writer = new StreamWriter(Stream);
        Reader = new StreamReader(Stream);

        logFile = new FileStream(RequestLogger.DirectoryName + '/' + logFileName, FileMode.CreateNew, FileAccess.Write);
        logFileWriter = new StreamWriter(logFile);

        Writer.AutoFlush = true;
        logFileWriter.AutoFlush = true;
    }

    public void Start()
    {
        if (!ReadConnectRequest())
        {
            WriteToLogFile("Failed to read connection request.");
            return;
        }

        proxyWrapper = new ProxyWrapper(ProxyAddress, ProxyPort);

        if (!proxyWrapper.Connect(logFileWriter))
        {
            WriteToLogFile("Proxy connect failed.");
            return;
        }

        if (!WriteProxyConnected())
        {
            WriteToLogFile("Failed to write connection response");
            return;
        }

        bool status;
        do {
            status = proxyWrapper.Interchange(Reader, Writer, logFileWriter);
        } while (status);

        WriteToLogFile("Interchange failed.");

        Clean();
    }

    private bool WriteProxyConnected()
    {
        try
        {
            Writer.WriteLine("HTTP/1.1 200 OK");
            Writer.WriteLine("");
            Writer.Flush();
        }
        catch
        {
            return false;
        }

        return true;
    }

    private bool ReadConnectRequest()
    {
        const string prefix = "CONNECT ";
        string firstLine = Reader.ReadLine();
        if (!firstLine.StartsWith(prefix))
        {
            return false;
        }

        string address = firstLine.Substring(prefix.Length);
        int space = address.IndexOf(' ');
        if (space < 0) return false;
        address = address.Substring(0, space);

        int colon = address.IndexOf(':');
        if (colon < 0) return false;
        ProxyAddress = address.Substring(0, colon);
        ProxyPort = ushort.Parse(address.Substring(colon + 1));

        WriteToLogFile("Address connecting to is '{0}' on port {1}", ProxyAddress, ProxyPort);

        // Skip connection lines.
        string line;
        while ((line = Reader.ReadLine()) != null && line.Length != 0)
        {}

        return true;
    }

    private void WriteToLogFile(string format, params object[] args)
    {
        string formatted = string.Format(format, args) + '\n';
        byte[] data = System.Text.Encoding.UTF8.GetBytes(formatted);
        logFile.Write(data, 0, data.Length);
    }

    public void Clean()
    {
        try
        {
            Stream.Close();
            logFile.Close();
        }
        catch
        {
        }
    }

}