using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;

public class HTTPServer : IDisposable
{
    public TcpListener Listener { get; protected set; }

    private readonly SecurityDetails securityDetails;

    public ushort Port { get; protected set; }

    public bool ShutdownRequested { get; protected set; }

    private Logger _logger;

    public HTTPServer(SecurityDetails securityDetails, ushort port)
    {
        Port = port;
        ShutdownRequested = false;

        this.securityDetails = securityDetails;

        _logger = new Logger(string.Format("HTTPServer(port={0})", port));
        _logger.Debug("Running on port {0}", port);
    }

    public void HandleNewClient(TcpClient tcpClient)
    {
        Stream stream = tcpClient.GetStream();

        string logFileName = CreateLogFileName();
        var remoteEndPoint = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
        _logger.Debug("New connection. (lport={0}, rip={1}, rport={2}, lf={3})",
                      Port,
                      remoteEndPoint.Address.ToString(),
                      remoteEndPoint.Port,
                      logFileName
        );
        
        if (securityDetails != null)
        {
            SslStream sstream = new SslStream(stream);
            stream = sstream;

            sstream.AuthenticateAsServer(securityDetails.Certificate, false, securityDetails.SecurityProtocols, false);
        }
        
        HTTPClient client = new HTTPClient(stream, logFileName);
        client.Start();
        client.Clean();
    }

    public void Start()
    {
        if (!OpenTCPServer())
        {
            return;
        }

        _logger.Debug("Ready!");

        try
        {
            while (!ShutdownRequested)
            {
                if (!Listener.Pending())
                {
                    // Thread.Sleep(Configuration.ListenerTimeout);
                    continue;
                }

                TcpClient client = Listener.AcceptTcpClient();
                new Thread(new ThreadStart(() => { HandleNewClient(client); })).Start();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("[HTTPServer] " + e.Message);
        }
        finally
        {
            DoShutdown();
        }
    }

    private string CreateLogFileName()
    {
        return DateTime.Now.ToString("o");
    }

    private bool OpenTCPServer()
    {
        try
        {
            Listener = new TcpListener(IPAddress.Any, (int)Port);
            Listener.Start();
        }
        catch (Exception e)
        {
            _logger.Error("Failed to start TCP server: {0}", e.Message);
            return false;
        }

        return true;
    }

    private void DoShutdown()
    {
        try
        {
            Listener.Stop();
            Listener = null;
        }
        catch
        {
        }
    }

    public void Dispose()
    {
        ShutdownRequested = true;
    }
}
