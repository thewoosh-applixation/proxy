using System;

struct RequestLogger
{
    public const string DirectoryName = "logs";
}

struct Logger {
    internal static void DoLog(string level, string tag, string format, params object[] args)
    {
        Console.WriteLine(string.Format(format, args));
    }

    // The name of the module or component
    public readonly string Tag;

    public Logger(string tag)
    {
        Tag = tag;
    }

    public void Debug(string format, params object[] args) => DoLog("Debug", Tag, format, args);
    public void Error(string format, params object[] args) => DoLog("Error", Tag, format, args);

}