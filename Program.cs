using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;

public static class Start
{
    public static bool UseSecureServer = false; // disabled for development;

    public static void Main()
    {
        // ensure logging directory exists
        Directory.CreateDirectory(RequestLogger.DirectoryName);

        if (UseSecureServer)
        {
            SecurityDetails details = new SecurityDetails("", "");
            var secureServer = new HTTPServer(details, 443);
            new Thread(new ThreadStart(secureServer.Start)).Start();
        }

        var nonSecureServer = new HTTPServer(null, 8080);
        new Thread(new ThreadStart(nonSecureServer.Start)).Start();
    }
}